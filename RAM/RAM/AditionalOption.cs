﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RAM
{    /// <summary>
     /// Дополнительный параметр объекта базы
     /// </summary>
    public class AditionalOption
    {
        public string Option { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

    }
}