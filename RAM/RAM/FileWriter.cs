﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAM
{
    /// <summary>
    /// Класс, отвечающий за запись результатов работы программы в файл
    /// </summary>
    public static class FileWriter
    {
        public static string GoodDataFileName { get; set; }
        public static string PartDataFileName { get; set; }
        public static string BadDataFileName { get; set; }

        static FileWriter()
        {
            GoodDataFileName = "data.txt";
            PartDataFileName = @"base_{0}.txt";
            BadDataFileName = "bad_data.txt";
        }
        /// <summary>
        /// Фильтрует получаемый список объектов, записывает данные в файл, в зависимости от корректности формата
        /// </summary>
        public static bool FormatAndWriteResult(List<DataBaseObject> baseObjects, string goodDataFilePath = "", string badDataFilePath = "", int partNumber = 5)
        {
            List<DataBaseObject> formattedBaseObjects = FormatEditor.FormatBaseObject(baseObjects);

            goodDataFilePath = Validator.ValidateDirectoryPath(goodDataFilePath);
            badDataFilePath = Validator.ValidateDirectoryPath(badDataFilePath);

            List<DataBaseObject> baseObjectTrusted = formattedBaseObjects.Where(element => !element.BadData).ToList();
            List<DataBaseObject> baseObjectBroken = formattedBaseObjects.Where(element => element.BadData).ToList();

            //Если необходимо просто вывести данные без разбиения на части
            //WriteResult(baseObjectTrusted, goodDataFileName, goodDataFilePath);
            var WritePartsTask = Task.Run(() => WriteResultByParts(baseObjectTrusted, PartDataFileName, partNumber, goodDataFilePath));
            var WriteBadDataTask = Task.Run(() => WriteResult(baseObjectBroken, BadDataFileName, badDataFilePath));

            Task.WaitAll(WritePartsTask, WriteBadDataTask);

            return true;
        }

        /// <summary>
        /// Записывает данные частями, в разные файлы
        /// </summary>
        private static void WriteResultByParts(List<DataBaseObject> baseObjectTrusted, string partDataFileName, int part, string dataFilePath = "")
        {
            IEnumerable<List<DataBaseObject>> baseObjectsParts = FormatEditor.Split(baseObjectTrusted, part);

            int partNumber = 1;

            foreach (List<DataBaseObject> baseObjectsPart in baseObjectsParts)
            {
                WriteResult(baseObjectsPart, string.Format(partDataFileName, partNumber), dataFilePath);
                partNumber++;
            }
        }

        /// <summary>
        /// Записывает все объекты из списка в файл
        /// </summary>
        private static void WriteResult(List<DataBaseObject> baseObjects, string fileName, string dataFilePath = "")
        {
            using (StreamWriter writer = new StreamWriter(dataFilePath + fileName, false, Encoding.UTF8))
            {
                foreach (DataBaseObject baseObject in baseObjects)
                {
                    Write(writer, baseObject);
                }
            }
        }

        /// <summary>
        /// Релизует запись в файл
        /// </summary>
        private static void Write(StreamWriter writer, DataBaseObject baseObject)
        {
            writer.WriteLine(baseObject.Name);
            writer.WriteLine(baseObject.Connect.Option);
            foreach (AditionalOption addOption in baseObject.AditionalOptions)
            {
                writer.WriteLine(addOption.Option);
            }
            writer.WriteLine();
        }
    }
}