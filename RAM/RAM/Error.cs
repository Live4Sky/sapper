﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RAM
{
    /// <summary>
    /// Ошибка, возникающая при неправильной работе
    /// </summary>
    class Error
    {
        public Error(int code)
        {
            ShowErrorDescription(code);
            Environment.Exit(code);
        }
        void ShowErrorDescription(int code)
        {
            Dictionary<int, string> ErrorDescriptions = new Dictionary<int, string>();
            ErrorDescriptions.Add(-1, "Файл отсутствует или его структура нарушена");
            ErrorDescriptions.Add(0, "Адрес файла не является корректным");

            if (ErrorDescriptions.ContainsKey(code))
            {
                Console.WriteLine(ErrorDescriptions[code]);
                Console.ReadKey();
                Environment.Exit(code);
            }
        }

    }
}