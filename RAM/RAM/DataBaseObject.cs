﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RAM
{
    /// <summary>
    /// Объект базы: основной элемент приложения
    /// </summary>
    public class DataBaseObject
    {
        public string Name { get; set; }
        public string Option { get; set; }
        public Connect Connect { get; set; }
        public List<AditionalOption> AditionalOptions { get; set; }
        public bool BadData { get; set; }
    }
}