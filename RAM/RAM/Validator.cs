﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace RAM
{
    /// <summary>
    /// Класс, отвечающий за валлидацию данный, проверяет данные на "правильность"
    /// </summary>
    public static class Validator
    {
        public static string BaseNamePattern { get; set; }
        public static string ParameterPattern { get; set; }
        public static string ConnectFileAddressPattern { get; set; }

        static Validator()
        {
            BaseNamePattern = @"\[(\S{1,})\]";
            ParameterPattern = @"(\S{1,})\=(\S*)";
            ConnectFileAddressPattern = @"^(?:[\w]\:|\\)(\\[a-z_\-\s0-9\.]+)+\.(txt|gif|jpg|exe|png|dll|sln|pdf|doc|docx|xls|xlsx)$";
        }
        /// <summary>
        /// Вызывает один из методов валидации согласно типу параметра Connect
        /// </summary>
        public static bool Validate(Connect connect)
        {
            switch (connect.Type)
            {
                case ConnectType.File:
                    return ValidateFilePath(connect);
                case ConnectType.Server:
                    return ValidateServerPath(connect);
                default:
                    return false;
            }
        }
        /// <summary>
        /// Разбирает параметр Connect на части, проверяет и присваивает корректные данные
        /// </summary>
        private static bool ValidateFilePath(Connect connect)
        {
            string option = connect.Option;

            //Проверка для файлов Windows
            if (ValidateWindowsFilePath(option))
            {
                return true;
            }

            //Проверка для сетевых ресурсов
            string filePath = FormatEditor.RemoveSpaces(option);
            filePath = filePath.Replace("Connect=File=", String.Empty);
            filePath = filePath.Replace("\"", String.Empty);

            connect.FilePath = filePath;

            return Regex.IsMatch(filePath, ConnectFileAddressPattern);
        }

        /// <summary>
        /// Проверяет путь к файлу в системе
        /// </summary>
        public static bool ValidateWindowsFilePath(string option)
        {
            return File.Exists(option);
        }

        /// <summary>
        /// Проверяет имя объекта базы
        /// </summary>
        public static bool ValidateBaseName(string baseName)
        {
            if (String.IsNullOrEmpty(baseName))
            {
                return false;
            }

            baseName = FormatEditor.RemoveSpaces(baseName);

            //Любые символы минимум 1
            return Regex.IsMatch(baseName, BaseNamePattern);
        }

        /// <summary>
        /// Проверяет дополнительный параметр
        /// </summary>
        public static bool ValidateAditionalOption(string aditionalOption)
        {
            if (String.IsNullOrEmpty(aditionalOption))
            {
                return false;
            }

            aditionalOption = FormatEditor.RemoveSpaces(aditionalOption);

            //Любые символы от 1, кроме пробела слева, и любые символы в правой части
            return Regex.IsMatch(aditionalOption, ParameterPattern);
        }
        /// <summary>
        /// Разбирает параметр Connect на части, проверяет и присваивает корректные данные
        /// </summary>
        private static bool ValidateServerPath(Connect connect)
        {
            string path = connect.Option;

            path = path.Replace(" ", string.Empty);

            if (path.Contains("Srvr=") && path.Contains("Ref="))
            {
                int firstServerIndex = path.IndexOf("Srvr=") + 5;
                int lastServerIndex = path.IndexOf(";");

                if (lastServerIndex == -1)
                {
                    return false;
                }

                int firstBaseIndex = path.IndexOf("Ref=") + 4;
                int lastBaseIndex = path.Length;

                string serverName = path.Substring(firstServerIndex, lastServerIndex - firstServerIndex);
                serverName = serverName.Replace("\"", string.Empty);

                string baseTitle = path.Substring(firstBaseIndex, lastBaseIndex - firstBaseIndex);
                baseTitle = baseTitle.Replace("\"", string.Empty);

                if (String.IsNullOrEmpty(serverName) || String.IsNullOrEmpty(baseTitle))
                {
                    return false;
                }

                connect.ServerName = serverName;
                connect.BaseTitle = baseTitle;
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Проверяет путь к директории в системе
        /// </summary>
        public static string ValidateDirectoryPath(string directoryPath)
        {
            if (!Directory.Exists(directoryPath))
            {
                return "";
            }

            return directoryPath;
        }

        /// <summary>
        /// Проверяет файл в системе на пустоту
        /// </summary>
        public static bool ValidateFileEmpty(string filePath)
        {
            return new StreamReader(filePath).ReadLine() != null;
        }
    }
}
