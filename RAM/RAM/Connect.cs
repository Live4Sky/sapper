﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RAM
{
    /// <summary>
    /// Обязательный параметр Connect объекта базы
    /// </summary>
    public class Connect
    {
        public ConnectType Type { get; set; }
        public string FilePath { get; set; }
        public string ServerName { get; set; }
        public string BaseTitle { get; set; }
        public string Option { get; set; }
    }
}