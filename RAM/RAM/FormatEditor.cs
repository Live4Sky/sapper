﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RAM
{
    /// <summary>
    /// Класс, отвечающий за форматирование информации
    /// </summary>
    public static class FormatEditor
    {
        public static string WriteBaseNamePattern { get; set; }
        public static string WriteAdditionalOptionPattern { get; set; }
        public static string WriteFileConnectOptionPattern { get; set; }
        public static string WriteServerConnectOptionPattern { get; set; }

        static FormatEditor()
        {
            WriteBaseNamePattern = "[{0}]";
            WriteAdditionalOptionPattern = "{0}={1}";
            WriteFileConnectOptionPattern = "Connect=File=\"{0}\";";
            WriteServerConnectOptionPattern = "Connect=Srvr=\"{0}\";Ref=\"{1}\";";
        }

        /// <summary>
        /// Удаляет пробелы в строке
        /// </summary>
        public static string RemoveSpaces(string element)
        {
            return element.Replace(" ", String.Empty);
        }

        /// <summary>
        /// Удаляет формат имени базы
        /// </summary>
        public static string RemoveFormatBaseName(string element)
        {
            element = RemoveExtremeChar(element);
            element = element.Replace("[", String.Empty);
            element = element.Replace("]", String.Empty);

            return element;
        }

        /// <summary>
        /// Удаляет ошибочные символы в экстремумах строки
        /// </summary>
        public static string RemoveExtremeChar(string element)
        {
            return element.Trim(' ', '*', '.');
        }

        /// <summary>
        /// Форматириует поля объекта согласно шаблонам(если объект неправильного формата, форматируем только имя базы)
        /// </summary>
        public static DataBaseObject FormatBaseObject(DataBaseObject baseObject)
        {
            if (baseObject.BadData)
            {
                return new DataBaseObject
                {
                    Name = FormatBaseName(baseObject.Name),
                    Connect = baseObject.Connect,
                    AditionalOptions = baseObject.AditionalOptions,
                    BadData = baseObject.BadData,
                    Option = baseObject.Option

                };
            }
            return new DataBaseObject
            {
                Name = FormatBaseName(baseObject.Name),
                Connect = FormatConnectOption(baseObject.Connect),
                AditionalOptions = FormatAdditionalOption(baseObject.AditionalOptions),
                BadData = baseObject.BadData,
                Option = baseObject.Option
            };
        }
        /// <summary>
        /// Форматирует поля всех объектов из переданной коллекции 
        /// </summary>
        public static List<DataBaseObject> FormatBaseObject(List<DataBaseObject> baseObjects)
        {
            List<DataBaseObject> formattedBaseObjects = new List<DataBaseObject>();
            foreach (DataBaseObject baseObject in baseObjects)
            {
                formattedBaseObjects.Add(FormatBaseObject(baseObject));
            }
            return formattedBaseObjects;
        }

        /// <summary>
        /// Форматирует имя объекта базы 
        /// </summary>
        public static string FormatBaseName(string element)
        {
            return String.Format(WriteBaseNamePattern, element);

        }

        /// <summary>
        /// Форматирует параметр Connect объекта базы
        /// </summary>
        public static Connect FormatConnectOption(Connect connectOption)
        {
            Connect formatedConnectOption = new Connect();

            if (connectOption.Type == ConnectType.File)
            {
                formatedConnectOption.Option = string.Format(WriteFileConnectOptionPattern, connectOption.FilePath);
            }
            else
            {
                formatedConnectOption.Option = string.Format(WriteServerConnectOptionPattern, connectOption.ServerName, connectOption.BaseTitle);
            }

            return formatedConnectOption;
        }

        /// <summary>
        /// Форматирует дополнительные параметры объекта базы
        /// </summary>
        public static AditionalOption FormatAdditionalOption(AditionalOption additionalOption)
        {
            AditionalOption formattedAdditionalOption = new AditionalOption();
            formattedAdditionalOption.Option = string.Format(WriteAdditionalOptionPattern, additionalOption.Key, additionalOption.Value);

            return formattedAdditionalOption;
        }
        /// <summary>
        /// Форматирует дополнительный параметр объекта базы
        /// </summary>
        public static List<AditionalOption> FormatAdditionalOption(List<AditionalOption> additionalOptions)
        {
            List<AditionalOption> formattedAdditionalOptions = new List<AditionalOption>();
            foreach (AditionalOption additionalOption in additionalOptions)
            {
                formattedAdditionalOptions.Add(FormatAdditionalOption(additionalOption));
            }

            return formattedAdditionalOptions;
        }

        /// <summary>
        /// Разделяет список объектов на указанное колличество частей
        /// </summary>
        public static IEnumerable<List<T>> Split<T>(this IEnumerable<T> list, int parts)
        {
            int i = 0;
            IEnumerable<List<T>> splits = from item in list
                                          group item by i++ % parts into part
                                          select part.ToList();
            return splits;
        }
    }
}