﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RAM
{
    /// <summary>
    /// Перечисление типа обязательного параметра Connect
    /// </summary>
    public enum ConnectType : int
    {
        File = 0,
        Server = 1
    }
}