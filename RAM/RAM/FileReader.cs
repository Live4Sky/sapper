﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace RAM
{
    /// <summary>
    /// Класс, отвечающий за считывание информации из файла и её редактировании
    /// </summary>
    class FileReader
    {
        /// <summary>
        /// Получает и редактирует информацию из файла
        /// </summary>
        public static List<DataBaseObject> OpenFileAndGetStrings(string filepath)
        {
            List<DataBaseObject> dataBaseObjects = new List<DataBaseObject>();
            DataBaseObject dataBaseObject = new DataBaseObject();
            List<AditionalOption> aditionalOptions = new List<AditionalOption>();

            string line;

            StreamReader sr = new StreamReader(filepath, Encoding.UTF8);
            while ((line = sr.ReadLine()) != null)
            {
                if (String.IsNullOrEmpty(line))
                {
                    VerifyBaseObjectNameOption(dataBaseObject);

                    SaveDataBaseObject(dataBaseObject, dataBaseObjects, aditionalOptions);

                    dataBaseObject = new DataBaseObject();
                    aditionalOptions = new List<AditionalOption>();

                    continue;
                }

                if (SetBaseObjectName(dataBaseObject, line)) { continue; }

                if (CheckOptionInBaseObject(dataBaseObject, aditionalOptions, line)) { continue; }

            }

            SaveDataBaseObject(dataBaseObject, dataBaseObjects, aditionalOptions);

            return CheckDataBaseObjects(dataBaseObjects);
        }

        /// <summary>
        /// Добавляет коллекцию необязательных параметров в объект базы, и добавляет сам объект в коллекцию объектнов
        /// </summary>
        private static void SaveDataBaseObject(DataBaseObject dataBaseObject, List<DataBaseObject> dataBaseObjects, List<AditionalOption> aditionalOptions)
        {
            dataBaseObject.AditionalOptions = aditionalOptions;
            dataBaseObjects.Add(dataBaseObject);
        }

        /// <summary>
        /// Устанавливает имя объекта базы
        /// </summary>
        private static bool SetBaseObjectName(DataBaseObject dataBaseObject, string line)
        {
            if (dataBaseObject.Name == null)
            {
                if (Validator.ValidateBaseName(line))
                {
                    dataBaseObject.Name = FormatEditor.RemoveFormatBaseName(line);
                    dataBaseObject.Option = line;
                    return true;
                }
                new Error((int)ErrorCode.BrokenFileError);
            }
            return false;
        }

        /// <summary>
        /// Проверяет и добавляет параметры в коллекцию
        /// </summary>
        private static bool CheckOptionInBaseObject(DataBaseObject dataBaseObject, List<AditionalOption> aditionalOptions, string line)
        {
            if (Validator.ValidateAditionalOption(line))
            {
                AddOptionInBaseObject(dataBaseObject, aditionalOptions, line);
                return true;
            }
            else
            {
                new Error((int)ErrorCode.BrokenFileError);
                return false;
            }
        }
        /// <summary>
        /// Разделяет по типам и добавляет параметры в коллекцию или в объект
        /// </summary>
        private static void AddOptionInBaseObject(DataBaseObject dataBaseObject, List<AditionalOption> aditionalOptions, string line)
        {
            if (line.Contains("Connect="))
            {
                Connect connect = new Connect();
                connect.Option = line;

                if (line.Contains("File="))
                {
                    connect.Type = ConnectType.File;
                }
                else if (line.Contains("Srvr="))
                {
                    connect.Type = ConnectType.Server;
                }
                else
                {
                    new Error((int)ErrorCode.BrokenFileError);
                }

                dataBaseObject.Connect = connect;
            }
            else
            {
                string divider = "=";
                AditionalOption aditionalOption = new AditionalOption();

                aditionalOption.Option = line;
                aditionalOption.Key = line.Substring(0, line.IndexOf(divider, StringComparison.Ordinal));
                aditionalOption.Value = line.Substring(line.IndexOf(divider, StringComparison.Ordinal) + divider.Length);

                aditionalOptions.Add(aditionalOption);
            }
        }

        /// <summary>
        /// Проверяет заполнены ли в объекте поля обязательные к заполнению
        /// </summary>
        private static void VerifyBaseObjectNameOption(DataBaseObject dataBaseObject)
        {
            if (dataBaseObject.Name == null || dataBaseObject.Connect == null)
            {
                // Нет имени базы или коннекта
                new Error((int)ErrorCode.BrokenFileError);
            }

        }
        /// <summary>
        /// Проверяет данные объекта и устанавливает флаг (BadData), если данные не соответствуют условиям форматирования
        /// </summary>
        public static List<DataBaseObject> CheckDataBaseObjects(List<DataBaseObject> dataBaseObjects)
        {
            foreach (DataBaseObject element in dataBaseObjects)
            {
                if (!Validator.Validate(element.Connect))
                {
                    element.BadData = true;
                }
            }

            return dataBaseObjects;
        }
    }
}
