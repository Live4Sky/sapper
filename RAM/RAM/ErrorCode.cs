﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RAM
{
    /// <summary>
    /// Коды ошибок при выполнении
    /// </summary>
    public enum ErrorCode : int
    {
        BrokenFileError = -1,
        BadFilePathArgument = 0
    }
}