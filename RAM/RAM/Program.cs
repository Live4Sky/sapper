﻿using System;
using System.Collections.Generic;

namespace RAM
{
    class Program
    {
        /// <summary>
        /// Главная точка входа для приложения
        /// </summary>
        static void Main(string[] args)
        {
            Initialize();

            string filePath = CheckAndGetPath(args);

            if (Validator.ValidateFileEmpty(filePath))
            {
                List<DataBaseObject> baseObjectsCollection = FileReader.OpenFileAndGetStrings(filePath);
                FileWriter.FormatAndWriteResult(baseObjectsCollection, "", "", 5);
            }
        }

        /// <summary>
        /// Проверяем файл по адресу, и возвращем адрес если всё в порядке
        /// </summary>
        private static string CheckAndGetPath(string[] args)
        {
            string filePath;
            //Если путь не был передан предоставлятся возможность указать его
            if (args.Length == 0)
            {
                Console.WriteLine("Не передан путь к файлу с данными, пожалуйста укажите имя файла, который находится в папке приложения");
                filePath = Environment.CurrentDirectory+"/"+Console.ReadLine();
            }
            else
            {
                filePath = args[0];
            }

            //Проверяем существование файла с данными
            bool correctFilePath = Validator.ValidateWindowsFilePath(filePath);
            if (!correctFilePath)
            {
                new Error((int)ErrorCode.BadFilePathArgument);
            }

            return filePath;
        }

        /// <summary>
        /// Инициализация всех параметров
        /// </summary>
        private static void Initialize()
        {
            InitValidatorPatterns();
            InitFormatEditorPatterns();
            InitFileNames();
        }

        /// <summary>
        /// Инициализация паттернов(шаблонов) валидатора(по умолчанию статиечский конструктор валидатора инициализирует такие же значения)
        /// </summary>
        private static void InitValidatorPatterns()
        {
            Validator.BaseNamePattern = @"\[(\S{1,})\]";
            Validator.ParameterPattern = @"(\S{1,})\=(\S*)";
            Validator.ConnectFileAddressPattern = @"^(?:[\w]\:|\\)(\\[a-z_\-\s0-9\.]+)+\.(txt|gif|jpg|exe|png|dll|sln|pdf|doc|docx|xls|xlsx)$";
        }

        /// <summary>
        /// Инициализация паттернов(шаблонов) записи(по умолчанию статиечский конструктор эдитора инициализирует такие же значения)
        /// </summary>
        private static void InitFormatEditorPatterns()
        {
            FormatEditor.WriteBaseNamePattern = "[{0}]";
            FormatEditor.WriteAdditionalOptionPattern = "{0}={1}";
            FormatEditor.WriteFileConnectOptionPattern = "Connect=File=\"{0}\";";
            FormatEditor.WriteServerConnectOptionPattern = "Connect=Srvr=\"{0}\";Ref=\"{1}\";";
        }

        /// <summary>
        /// Инициализация имён файлов записи
        /// </summary>
        private static void InitFileNames()
        {
            FileWriter.GoodDataFileName = "data.txt";
            FileWriter.PartDataFileName = @"base_{0}.txt";
            FileWriter.BadDataFileName = "bad_data.txt";
        }
    }
}